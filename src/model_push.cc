/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>

namespace gazebo
{
  class ModelPush : public ModelPlugin
  {
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // Store the pointer to the model
      this->model = _parent;

      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&ModelPush::OnUpdate, this));
      link=model->GetChildLink("base_link");
      _joint_motor_0=model->GetJoint("motor_0_joint");
      _world=model->GetWorld();

      fichero=fopen("/home/isidro/google-drive/TFG/tilt-rotor-matlab/prueba_fichero.txt","w");
    }

    // Called by the world update start event
    public: void OnUpdate()
    {
      // Apply a small linear velocity to the model.
      //this->link->AddLinkForce(ignition::math::Vector3d(0, 0.00, 0),ignition::math::Vector3d(0, 0, 1));
      //
      if (_world->GetSimTime().Float()>time_ant+0.01){

         fprintf(fichero,"%f\t", _world->GetSimTime().Float());

         math::Vector3 medida=this->link->GetRelativeAngularVel();
         fprintf(fichero,"%f\t", medida.x);
         fprintf(fichero,"%f\t", medida.y);
         fprintf(fichero,"%f\t", medida.z);

         medida=this->link->GetRelativeForce();
         fprintf(fichero,"%f\t", medida.x);
         fprintf(fichero,"%f\t", medida.y);
         fprintf(fichero,"%f\t", medida.z);

         medida=this->link->GetRelativeLinearAccel();
         fprintf(fichero,"%f\t", medida.x);
         fprintf(fichero,"%f\t", medida.y);
         fprintf(fichero,"%f\t", medida.z);

         medida=this->link->GetRelativeLinearVel();
         fprintf(fichero,"%f\t", medida.x);
         fprintf(fichero,"%f\t", medida.y);
         fprintf(fichero,"%f\t", medida.z);

         double vel_joint_0_x=this->_joint_motor_0->GetVelocity(0);
         double vel_joint_0_y=this->_joint_motor_0->GetVelocity(1);
         double vel_joint_0_z=this->_joint_motor_0->GetVelocity(2);
         fprintf(fichero,"%f\t", vel_joint_0_x);
         fprintf(fichero,"%f\t", vel_joint_0_y);
         fprintf(fichero,"%f\t", vel_joint_0_z);
         math::Angle ang_joint_0_x=this->_joint_motor_0->GetAngle(0);
         math::Angle ang_joint_0_y=this->_joint_motor_0->GetAngle(1);
         math::Angle ang_joint_0_z=this->_joint_motor_0->GetAngle(2);
         fprintf(fichero,"%f\t", ang_joint_0_x.Radian());
         fprintf(fichero,"%f\t", ang_joint_0_y.Radian());
         fprintf(fichero,"%f\t", ang_joint_0_z.Radian());


         // En la api indica que el indice no sirve
         physics::JointWrench fuerza_par=_joint_motor_0->GetForceTorque(0); 
         medida=fuerza_par.body1Force;
         fprintf(fichero,"%f\t", medida.x);
         fprintf(fichero,"%f\t", medida.y);
         fprintf(fichero,"%f\t", medida.z);
         medida=fuerza_par.body1Torque;
         fprintf(fichero,"%f\t", medida.x);
         fprintf(fichero,"%f\t", medida.y);
         fprintf(fichero,"%f\t", medida.z);

         fprintf(fichero,"\n");
         time_ant=_world->GetSimTime().Float();
      }
    }

    // Pointer to the model
    private: physics::ModelPtr model;
    private: physics::LinkPtr link;
    private: physics::JointPtr _joint_motor_0;
    private: physics::WorldPtr _world;


    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;
    private: FILE *fichero;
    private: float time_ant=0;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ModelPush)
}
